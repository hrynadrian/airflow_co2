from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.hooks import PostgresHook
import requests
from datetime import datetime
import pandas as pd
import numpy as np


class CO2Handler:

    def __init__(self):
        self.data = []

    def process(self, **kwargs):
        urls = ["http://192.168.87.17:8088/", "http://192.168.87.18:8088/"]
        index_sensor = 1
        for url in urls:
            r = requests.get(url=url)
            dict_ = r.json()
            dict_["sensor_ind"] = index_sensor
            self.to_dataframe(dict_)
            index_sensor += 1

        filename = "co2_data.csv"
        self.safe_file(filename)

        task_instance = kwargs['ti']
        task_instance.xcom_push(key='path_str', value=filename)

    def to_dataframe(self, dict_):
        data = pd.DataFrame([dict_])

        data["MHZ14A_CO2"] = data["MHZ14A_CO2"].astype(int)
        data["DHT22_T"] = data["DHT22_T"].astype(float)
        data["DHT22_H"] = data["DHT22_H"].astype(float)

        data.rename(columns={'DHT22_T': 'temperature', 'DHT22_H': 'humidity', 'MHZ14A_CO2': 'co2_level'}, inplace=True)

        cols = [('co2_level', 1), ('temperature', 2), ('humidity', 3)]

        values = np.array([data[i[0]] for i in cols])
        metric_ids = np.array([i[1] for i in cols])
        ts = np.array([datetime.now().strftime("%Y-%m-%d %H:%M:%S") for i in range(3)])
        sensor_ids = [int(data['sensor_ind']) for _ in range(3)]

        df = pd.DataFrame()

        df["ts"] = ts
        df['sensor_id'] = sensor_ids
        df["metric_id"] = metric_ids
        df["observed_value"] = values

        self.data.append(df)

    def insert(self, **kwargs):
        task_instance = kwargs['ti']
        filename = task_instance.xcom_pull(task_ids="read_and_transform", key='path_str')

        data = pd.read_csv(filename, parse_dates=['ts'])
        del data['Unnamed: 0']
        hook = PostgresHook(postgres_conn_id='postgres_default')
        hook.insert_rows(table='observations',
                         target_fields=list(data),
                         rows=[tuple(x) for x in data.values])

    def safe_file(self, filename):
        concated_df = pd.concat(self.data)
        concated_df.to_csv(filename)


co2 = CO2Handler()

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2019, 8, 17, 18, 00),
}


dag = DAG('co2_temperature_humidity', schedule_interval='*/2 * * * *', default_args=default_args, catchup=False)

readtransform = PythonOperator(
    task_id='read_and_transform',
    python_callable=co2.process,
    provide_context=True,
    dag=dag)


insert_to_db = PythonOperator(
    task_id='insert_to_db',
    python_callable=co2.insert,
    provide_context=True,
    dag=dag
)

readtransform.set_downstream(insert_to_db)